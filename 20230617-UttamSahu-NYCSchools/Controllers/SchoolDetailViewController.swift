//
//  SchoolDetailViewController.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 18/06/23.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    
    public var dbn: String = ""
    private let activityIndicatorView = UIActivityIndicatorView(style: .large)
    
    var schoolSATScoreViewModelArray = [SchoolViewModel]()
    private let cellReuseIdentifier = "SchoolDetailViewController"
    
    private var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "School Details"
        // in your viewDidLoad or viewWillAppear
        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "", style: .plain, target: nil, action: nil)
        setupUI()
        fetchData()
    }
    
    private func fetchData() {
        activityIndicatorView.startAnimating()
        let dataOperator = SchoolDataOperator()
        // Getting school data from API
        // Once data receive
        dataOperator.getSchoolSATScoresData(url: Constants.schoolSATScoreURL + dbn) {[weak self] (schoolSATScoreVMArray, error) in
            // This will stop the activity indicator
            self?.activityIndicatorView.stopAnimating()
            
            if let _schoolSATScoreVMArray = schoolSATScoreVMArray, _schoolSATScoreVMArray.count > 0 {
                self?.schoolSATScoreViewModelArray[0].numOfSATTestTakers = _schoolSATScoreVMArray[0].numOfSATTestTakers
                self?.schoolSATScoreViewModelArray[0].satMathAvgScore = _schoolSATScoreVMArray[0].satMathAvgScore
                self?.schoolSATScoreViewModelArray[0].satCriticalReadingAvgScore = _schoolSATScoreVMArray[0].satCriticalReadingAvgScore
                self?.schoolSATScoreViewModelArray[0].satWritingAvgScore = _schoolSATScoreVMArray[0].satWritingAvgScore
                
            }
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
            
        }
    }
    
    private func showAlert() {
        let alertController = UIAlertController(
            title: "No Data",
            message: "School's data is not available.",
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    private func setupUI() {
        
        // Create the UITableView
        tableView = UITableView(frame: view.bounds, style: .plain)
        tableView.dataSource = self
        tableView.register(SchoolSATScoreTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        view.addSubview(tableView)
        
        // Setup activityIndicatorView
        activityIndicatorView.color = .gray
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicatorView)
        
        activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

extension SchoolDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolSATScoreViewModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! SchoolSATScoreTableViewCell
        if schoolSATScoreViewModelArray.count > 0 {
            cell.configure(with: schoolSATScoreViewModelArray[indexPath.row])
        }
        
        return cell
    }
    
    
}
