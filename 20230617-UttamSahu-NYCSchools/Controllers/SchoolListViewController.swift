//
//  SchoolListViewController.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 17/06/23.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    var schoolViewModels: [SchoolViewModel] = []
    var tableView: UITableView!
    private let cellReuseIdentifier = "SchoolTableViewCellIdentifier"
    private let activityIndicatorView = UIActivityIndicatorView(style: .large)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "SCHOOL LIST"
        // Setup tableview
        setupUI()
        // Reload data
        fetchData()
    }
    
    private func setupUI() {
        // Create the UITableView
        tableView = UITableView(frame: view.bounds, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SchoolTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        view.addSubview(tableView)
        
        // Customize UITableView appearance
        tableView.backgroundColor = .white
        tableView.separatorStyle = .singleLine
        
        // Setup activityIndicatorView
        activityIndicatorView.color = .gray
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicatorView)
        
        activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    private func fetchData() {
        activityIndicatorView.startAnimating()
        let dataOperator = SchoolDataOperator()
        // Getting school data from API
        // Once data receive
        dataOperator.getSchoolData(url: Constants.schoolDataURL) {[weak self] (schoolViewModelArray, error) in
            // This will stop the activity indicator
            self?.activityIndicatorView.stopAnimating()
            
            if let _schoolViewModelArray = schoolViewModelArray, _schoolViewModelArray.count > 0 {
                self?.schoolViewModels = _schoolViewModelArray
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            } else {
                // Handle error case
                self?.showAlert()
            }
            
            
        }
    }
    
    private func showAlert() {
        let alertController = UIAlertController(
            title: "No Data",
            message: "School's data is not available.",
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension SchoolListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! SchoolTableViewCell
        cell.configure(with: schoolViewModels[indexPath.row])
        
        cell.delegate = self
        
        return cell
    }
}

extension SchoolListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let viewController = SchoolDetailViewController()
        viewController.dbn = self.schoolViewModels[indexPath.row].dbn ?? ""
        viewController.schoolSATScoreViewModelArray.append(self.schoolViewModels[indexPath.row])
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension SchoolListViewController : SchoolDelegate {
    func didReceiveVisitWebsite(url: URL?) {
        guard let websiteURL = url else {
            return
        }
        let viewController = SchoolWebsiteViewController(websiteURL: websiteURL)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
