//
//  SchoolWebsiteViewController.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 17/06/23.
//

import UIKit
import WebKit

class SchoolWebsiteViewController: UIViewController, WKNavigationDelegate {
    private var webView: WKWebView!
    private var websiteURL: URL!
    private let activityIndicatorView = UIActivityIndicatorView(style: .large)
    
    init(websiteURL: URL) {
        super.init(nibName: nil, bundle: nil)
        self.websiteURL = websiteURL
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "", style: .plain, target: nil, action: nil)
        // Setup UI
        setupUI()
    }
    
    func setupUI() {
        
        // Setup webView
        webView = WKWebView(frame: view.bounds)
        webView.navigationDelegate = self
        view.addSubview(webView)
        let request = URLRequest(url: websiteURL)
        webView.load(request)
        
        // Setup activityIndicatorView
        activityIndicatorView.color = .gray
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicatorView)
        
        activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    private func showAlert() {
        let alertController = UIAlertController(
            title: "Under Maintenance",
            message: "Website is under maintenance, Visit after sometime!",
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicatorView.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicatorView.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicatorView.stopAnimating()
    }
}


