//
//  SchoolViewModel.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 17/06/23.
//

import Foundation

struct SchoolViewModel: Decodable {
    
    var dbn: String?
    var schoolName: String?
    var aboutSchool: String?
    var seats: String?
    var opportunities: String?
    var opportunities2: String?
    var website: String?
    
    var address: String?
    var extracurricular_activities: String?
    
    var numOfSATTestTakers: String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore: String?
    var satWritingAvgScore: String?
    
    init(school: SchoolModel) {
        self.dbn = school.dbn
        self.schoolName = school.school_name
        self.aboutSchool = school.overview_paragraph
        self.seats = school.school_10th_seats
        self.opportunities = school.academicopportunities1
        self.website = school.website
        self.opportunities2 = school.academicopportunities2
        
        self.address = school.location
        self.extracurricular_activities = school.extracurricular_activities
    }
    
    
}
