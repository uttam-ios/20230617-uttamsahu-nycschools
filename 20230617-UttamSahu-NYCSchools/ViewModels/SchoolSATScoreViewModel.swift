//
//  SchoolSATScoreViewModel.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 18/06/23.
//

import Foundation

struct SchoolSATScoreViewModel: Decodable {
    var dbn: String?
    var schoolName: String?
    var numOfSATTestTakers: String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore: String?
    var satWritingAvgScore: String?
    
    init(schoolSATScoreModel: SchoolSATScoreModel) {
        self.dbn = schoolSATScoreModel.dbn
        self.schoolName = schoolSATScoreModel.school_name
        self.numOfSATTestTakers = schoolSATScoreModel.num_of_sat_test_takers
        self.satCriticalReadingAvgScore = schoolSATScoreModel.sat_critical_reading_avg_score
        self.satMathAvgScore = schoolSATScoreModel.sat_math_avg_score
        self.satWritingAvgScore = schoolSATScoreModel.sat_writing_avg_score
    }
}
