//
//  Constants.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 18/06/23.
//

import Foundation

struct Constants {
    static let schoolDataURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let schoolSATScoreURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}
