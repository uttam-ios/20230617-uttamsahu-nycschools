//
//  NetworkManager.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 17/06/23.
//

import Foundation

class NetworkManager {
    
    static let shared = NetworkManager()
    private init() {}
    
    func makeAPIRequest(url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                let unknownError = NSError(domain: "", code: -1, userInfo: nil)
                completion(.failure(unknownError))
                return
            }
            
            guard (200...299).contains(httpResponse.statusCode) else {
                let httpError = NSError(domain: "", code: httpResponse.statusCode, userInfo: nil)
                completion(.failure(httpError))
                return
            }
            
            if let data = data {
                completion(.success(data))
            } else {
                let unknownError = NSError(domain: "", code: -1, userInfo: nil)
                completion(.failure(unknownError))
            }
        }
        
        task.resume()
    }
}
