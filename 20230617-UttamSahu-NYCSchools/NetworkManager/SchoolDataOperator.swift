//
//  SchoolDataOperator.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 18/06/23.
//

import Foundation


class SchoolDataOperator {
    
    func getSchoolData(url: String, completion: @escaping ([SchoolViewModel]?, Error?) -> Void) {
        if let url = URL(string: url) {
            NetworkManager.shared.makeAPIRequest(url: url) { result in
                switch result {
                case .success(let data):
                    do {
                        let decoder = JSONDecoder()
                        let schoolArray = try decoder.decode([SchoolModel].self, from: data)
                        var schoolViewModelArray = [SchoolViewModel]()
                        for school in schoolArray {
                            let schoolViewModel = SchoolViewModel(school: school)
                            schoolViewModelArray.append(schoolViewModel)
                        }
                        DispatchQueue.main.async {
                            completion(schoolViewModelArray,nil)
                        }
                        
                    } catch {
                        DispatchQueue.main.async {
                            completion(nil,error)
                        }
                    }
                    
                case .failure(let error):
                    DispatchQueue.main.async {
                        completion(nil,error)
                    }
                    print(error)
                }
            }
        }
    }
    
    func getSchoolSATScoresData(url: String, completion: @escaping ([SchoolSATScoreViewModel]?, Error?) -> Void) {
        if let url = URL(string: url) {
            NetworkManager.shared.makeAPIRequest(url: url) { result in
                switch result {
                case .success(let data):
                    do {
                        let decoder = JSONDecoder()
                        let schoolArray = try decoder.decode([SchoolSATScoreModel].self, from: data)
                        
                        var schoolViewModelArray = [SchoolSATScoreViewModel]()
                        for satSoreModel in schoolArray {
                            let schoolViewModel = SchoolSATScoreViewModel(schoolSATScoreModel: satSoreModel)
                            schoolViewModelArray.append(schoolViewModel)
                        }
                        
                        DispatchQueue.main.async {
                            completion(schoolViewModelArray, nil)
                        }
                        
                    } catch {
                        DispatchQueue.main.async {
                            completion(nil,error)
                        }
                    }
                    
                case .failure(let error):
                    DispatchQueue.main.async {
                        completion(nil,error)
                    }
                }
            }
        }
    }
    
    
}
