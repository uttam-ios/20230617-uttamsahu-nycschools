//
//  SchoolTableViewCell.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 17/06/23.
//

import UIKit

/*
 * In this example, we create a custom SchoolTableViewCell subclass of UITableViewCell. Inside the class, we define the necessary UI elements (nameLabel, aboutLabel, seatsLabel, academicOpportunitiesLabel) and configure their appearance.

 * The setupUI() method sets up the layout constraints for the UI elements within the cell's contentView.

 * The configure(with:) method is used to populate the cell's content with data. It takes a School object as a parameter and sets the appropriate values for each UI element.

 * You can customize the font, layout, or appearance of the labels according to your needs.

 * To use this custom cell in your UITableView, you can register it in the viewDidLoad method of your view controller:
 */

protocol SchoolDelegate : AnyObject {
    func didReceiveVisitWebsite(url: URL?)
}

class SchoolTableViewCell: UITableViewCell {
    
    weak var delegate: SchoolDelegate?
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let aboutLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let seatsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let academicOpportunitiesLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let websiteButton: UIButton = {
        let button = UIButton()
        button.setTitle("Visit Website", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.contentHorizontalAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var schoolWebsiteURL: URL?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
        
        // Add action for website button tap
        websiteButton.addTarget(self, action: #selector(websiteButtonTapped), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(aboutLabel)
        contentView.addSubview(websiteButton)
        
        // Constraints
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            aboutLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            aboutLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            aboutLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            websiteButton.topAnchor.constraint(equalTo: aboutLabel.bottomAnchor, constant: 8),
            websiteButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            websiteButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            websiteButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
    
    func configure(with school: SchoolViewModel) {
        nameLabel.text = school.schoolName
        aboutLabel.text = school.aboutSchool
        
        websiteButton.isHidden = school.website == nil
        if var website = school.website {
            // Replacing from www. to https://
            // checking if https:// is available on website or not, if not just adding it.
            if website.contains("www.") {
                website = website.replacing("www.", with: "https://")
            } else if !website.contains("https://") {
                website = "https://" + website
            }
            schoolWebsiteURL = URL(string: website)
        }
        
    }
    
        @objc private func websiteButtonTapped() {
            delegate?.didReceiveVisitWebsite(url: schoolWebsiteURL)

        }
}

// Make private property accessable :
/*
 In this example, we create a test case called testPrivateLabelProperty. Inside the test case, we create an instance of SchoolTableViewCell and use an extension on SchoolTableViewCell to access the private label property label using a helper method getNameLabel(). We then perform assertions on the private label to ensure it meets the expected conditions.

 Again, it's important to note that testing private properties directly should be done sparingly and with caution. It's generally recommended to focus on testing the public interface and behavior of your classes.

 */

extension SchoolTableViewCell {
    func value<T>(for keyPath: KeyPath<SchoolTableViewCell, T>) -> T {
        return self[keyPath: keyPath]
    }
    
    func getNameLabel() -> UILabel {
        return self.nameLabel
    }
    
    func getAboutLabel() -> UILabel {
        return self.aboutLabel
    }
    
    func getSeatLabel() -> UILabel {
        return self.seatsLabel
    }
    
    func getAcademicOpportunitiesLabel() -> UILabel {
        return self.academicOpportunitiesLabel
    }
    
    func getWebsiteButton() -> UIButton {
        return self.websiteButton
    }
}

