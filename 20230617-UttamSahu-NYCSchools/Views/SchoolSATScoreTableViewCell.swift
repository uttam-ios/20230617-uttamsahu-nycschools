//
//  SchoolSATScoreTableViewCell.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 18/06/23.
//

import UIKit

class SchoolSATScoreTableViewCell: UITableViewCell {
    // Create UILabels for displaying school data
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        return label
    }()
    
    private let aboutLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let seatsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let academicOpportunitiesLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let testTakerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    private let bgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 5.0
        return view
    }()
    
    private let mathLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    private let readingLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    private let writingLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    // Override the initializer to set up the cell's layout
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
        
    }
    
    func setupUI() {
        // Add subviews to the cell's content view
        contentView.addSubview(nameLabel)
        contentView.addSubview(aboutLabel)
        contentView.addSubview(seatsLabel)
        contentView.addSubview(academicOpportunitiesLabel)
        contentView.addSubview(testTakerLabel)
        contentView.addSubview(bgView)
        
        let leading: CGFloat = 16
        // Set up constraints for the labels
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leading),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -leading)
        ])
        
        NSLayoutConstraint.activate([
            aboutLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: leading),
            aboutLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leading),
            aboutLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -leading)
        ])
        
        NSLayoutConstraint.activate([
            seatsLabel.topAnchor.constraint(equalTo: aboutLabel.bottomAnchor, constant: leading),
            seatsLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leading),
            seatsLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -leading)
        ])
        
        NSLayoutConstraint.activate([
            academicOpportunitiesLabel.topAnchor.constraint(equalTo: seatsLabel.bottomAnchor, constant: leading),
            academicOpportunitiesLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leading),
            academicOpportunitiesLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -leading)
        ])
        
        NSLayoutConstraint.activate([
            testTakerLabel.topAnchor.constraint(equalTo: academicOpportunitiesLabel.bottomAnchor, constant: 10),
            testTakerLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leading),
            testTakerLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -leading),
            testTakerLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        NSLayoutConstraint.activate([
            bgView.topAnchor.constraint(equalTo: testTakerLabel.bottomAnchor, constant: 10),
            bgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leading),
            bgView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -leading),
            bgView.heightAnchor.constraint(equalToConstant: 60),
            bgView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30)
        ])
        
        let stackView = UIStackView(arrangedSubviews: [mathLabel, createSeparatorView(),readingLabel, createSeparatorView(), writingLabel])
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        
        bgView.addSubview(stackView)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: bgView.topAnchor, constant: 1),
            stackView.leadingAnchor.constraint(equalTo: bgView.leadingAnchor, constant: 35),
            stackView.trailingAnchor.constraint(equalTo: bgView.trailingAnchor, constant: -35),
            stackView.bottomAnchor.constraint(equalTo: bgView.bottomAnchor, constant: -1)
        ])

    }
    
    
    func createSeparatorView() -> UIView {
        let separator = UIView()
        separator.backgroundColor = .gray
        separator.widthAnchor.constraint(equalToConstant: 1).isActive = true // Adjust the width of the separator
        return separator
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Configure the cell with School data
    func configure(with schoolVM: SchoolViewModel) {
        nameLabel.text = schoolVM.schoolName
        aboutLabel.text = schoolVM.aboutSchool

//        let keyAttribute = [ NSAttributedString.Key.font: UIFont(name: "Helvetica-bold", size: 14.0)! ]
//        let valueAttribute = [ NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 14.0)! ]
//
//        let textString = NSMutableAttributedString(string: "Seats:\(schoolVM.seats ?? "NA")", attributes: keyAttribute )
//
//        let valueString = NSMutableAttributedString(string: "\(schoolVM.seats ?? "NA")", attributes: valueAttribute )
        
        
        seatsLabel.attributedText = getAttributtedString(key: "Seats", value: schoolVM.seats ?? "NA")

        var opportunity = "\n\n" + (schoolVM.opportunities ?? "") + "\n" + (schoolVM.opportunities2 ?? "") + "\n\n"
        
        var attributtedString = NSMutableAttributedString()
        
        let opportunityAttributed = getAttributtedString(key: "Opportunities", value: opportunity)
        
        attributtedString.append(opportunityAttributed)
        
        let addreddText = "\n\n" + (schoolVM.address ?? "")
        let addressAttributed = getAttributtedString(key: "Address", value: addreddText)
        
        attributtedString.append(addressAttributed)
        
//        if let address = schoolVM.address {
//            var addresstext = getAttributtedString(key: "Address", value: address)
//            opportunity = "\n\n" + opportunity + "\n\n" + "Address:\n\n \(address)"
//        }
//        if let extracurricular = schoolVM.extracurricular_activities {
//            opportunity =  opportunity + "Extracurricular activities:\n \(extracurricular)"
//        }
        
        academicOpportunitiesLabel.attributedText = attributtedString//getAttributtedString(key: "Academic Opportunities", value: opportunity)
                
        testTakerLabel.attributedText = getAttributtedString(key: "SAT Test Taker", value: schoolVM.numOfSATTestTakers ?? "NA")
        
        
        mathLabel.text = "Math\n\(schoolVM.satMathAvgScore ?? "NA")"
        readingLabel.text = "Reading\n\(schoolVM.satCriticalReadingAvgScore ?? "NA")"
        writingLabel.text = "Writing\n\(schoolVM.satWritingAvgScore ?? "NA")"
    }
}

func getAttributtedString(key: String, value: String)->NSAttributedString {
    let keyAttribute = [ NSAttributedString.Key.font: UIFont(name: "Helvetica-bold", size: 14.0)! ]
    let valueAttribute = [ NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 14.0)! ]
    
    let textString = NSMutableAttributedString(string: "\(key):", attributes: keyAttribute )
    
    let valueString = NSMutableAttributedString(string: "\(value)", attributes: valueAttribute )
    textString.append(valueString)
    return textString
}

// Make private property accessable :
/*
 In this example, we create a test case called testPrivateLabelProperty. Inside the test case, we create an instance of SchoolTableViewCell and use an extension on SchoolTableViewCell to access the private label property label using a helper method getNameLabel(). We then perform assertions on the private label to ensure it meets the expected conditions.

 Again, it's important to note that testing private properties directly should be done sparingly and with caution. It's generally recommended to focus on testing the public interface and behavior of your classes.

 */
extension SchoolSATScoreTableViewCell {
    
    func getNameLabel() -> UILabel {
        return self.nameLabel
    }
    
    func getAboutLabel() -> UILabel {
        return self.aboutLabel
    }
    
    func getSeatLabel() -> UILabel {
        return self.seatsLabel
    }
    
    func getAcademicOpportunitiesLabel() -> UILabel {
        return self.academicOpportunitiesLabel
    }
    func getTestTakerLabel() -> UILabel {
        return self.testTakerLabel
    }
    
    func getMathLabel() -> UILabel {
        return self.mathLabel
    }
    
    func getreadingLabel() -> UILabel {
        return self.readingLabel
    }
    
    func getWritingLabel() -> UILabel {
        return self.writingLabel
    }
    
}
