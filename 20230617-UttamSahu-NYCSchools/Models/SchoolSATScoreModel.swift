//
//  SchoolSATScoreModel.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 18/06/23.
//

import Foundation


struct SchoolSATScoreModel: Decodable {
    var dbn: String?
    var school_name: String?
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
}
