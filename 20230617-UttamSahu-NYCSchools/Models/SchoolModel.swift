//
//  SchoolModel.swift
//  20230617-UttamSahu-NYCSchools
//
//  Created by Uttam Sahu on 17/06/23.
//

import Foundation

struct SchoolModel: Decodable {
    var dbn: String?
    var school_name: String?
    var overview_paragraph: String?
    var school_10th_seats: String?
    var academicopportunities1: String?
    var academicopportunities2: String?
    var location: String?
    var website: String?
    
    var extracurricular_activities: String?
    
    
}
