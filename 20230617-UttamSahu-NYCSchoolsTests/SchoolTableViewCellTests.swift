//
//  SchoolTableViewCellTests.swift
//  20230617-UttamSahu-NYCSchoolsTests
//
//  Created by Uttam Sahu on 18/06/23.
//

import XCTest
@testable import _0230617_UttamSahu_NYCSchools

class SchoolTableViewCellTests: XCTestCase {
    
    var tableView: UITableView!
    var cell: SchoolTableViewCell!
    var cellId = "SchoolTableViewCell"
    
    override func setUp() {
        super.setUp()
        
        tableView = UITableView()
        tableView.register(SchoolTableViewCell.self, forCellReuseIdentifier: cellId)
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? SchoolTableViewCell
    }
    
    override func tearDown() {
        tableView = nil
        cell = nil
        
        super.tearDown()
    }
    
    func testConfigureCell() {
        // Create a sample school object
        var school = SchoolModel()
        school.school_name = "Liberation Diploma Plus High School"
        school.overview_paragraph = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform."
        school.school_10th_seats = "5"
        school.academicopportunities1 = "Sample Academic Opportunities"
        school.website = "www.theclintonschool.net"
        
        let schoolVM = SchoolViewModel(school: school)
        
        // Configure the cell with the sample school data
            cell.configure(with: schoolVM)
            
        // Assert that the cell's UI elements are updated correctly
        XCTAssertEqual(cell.getNameLabel().text, "Liberation Diploma Plus High School")
        XCTAssertEqual(cell.getAboutLabel().text, "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform.")
        XCTAssertFalse(cell.getWebsiteButton().isHidden)
    }
}









