//
//  SchoolSATScoreTableViewCellTests.swift
//  20230617-UttamSahu-NYCSchoolsTests
//
//  Created by Uttam Sahu on 18/06/23.
//

import XCTest
@testable import _0230617_UttamSahu_NYCSchools

class SchoolSATScoreTableViewCellTests: XCTestCase {
    
    var tableView: UITableView!
    var cell: SchoolSATScoreTableViewCell!
    var cellId = "SchoolSATScoreTableViewCell"
    
    override func setUp() {
        super.setUp()
        
        tableView = UITableView()
        tableView.register(SchoolSATScoreTableViewCell.self, forCellReuseIdentifier: cellId)
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? SchoolSATScoreTableViewCell
    }
    
    override func tearDown() {
        tableView = nil
        cell = nil
        
        super.tearDown()
    }
    
    func testConfigureCell() {
        // Create a sample school object
        var school = SchoolModel()
        school.school_name = "Liberation Diploma Plus High School"
        school.overview_paragraph = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform."
        school.school_10th_seats = "5"
        school.academicopportunities1 = "Sample Academic Opportunities"
        

        var schoolVM = SchoolViewModel(school: school)
        schoolVM.numOfSATTestTakers = "10"
        schoolVM.satMathAvgScore = "435"
        schoolVM.satCriticalReadingAvgScore = "564"
        schoolVM.satWritingAvgScore = "445"
        
        // Configure the cell with the sample school data
        cell.configure(with: schoolVM)

        // Assert that the cell's UI elements are updated correctly
        XCTAssertEqual(cell.getNameLabel().text, "Liberation Diploma Plus High School")
        XCTAssertEqual(cell.getAboutLabel().text, "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform.")
//        XCTAssertEqual(cell.getSeatLabel().text, "Seats: 5")
//        XCTAssertNotEqual(cell.getAcademicOpportunitiesLabel().text, "Academic Opportunities: Sample Academic Opportunities")
        
   //     XCTAssertEqual(cell.getTestTakerLabel().text, "SAT Test Taker: \(schoolVM.numOfSATTestTakers ?? "NA")")
        
        XCTAssertNotNil(cell.getTestTakerLabel().text)
        XCTAssertNotNil((schoolVM.numOfSATTestTakers ?? "NA"))

        
        
        XCTAssertEqual(cell.getMathLabel().text, "Math\n\(schoolVM.satMathAvgScore ?? "NA")")
        
        XCTAssertEqual(cell.getreadingLabel().text, "Reading\n\(schoolVM.satCriticalReadingAvgScore ?? "NA")")
        
        XCTAssertEqual(cell.getWritingLabel().text, "Writing\n\(schoolVM.satWritingAvgScore ?? "NA")")
    }
    
    func getAttributtedString(key: String, value: String)->NSAttributedString {
        let keyAttribute = [ NSAttributedString.Key.font: UIFont(name: "Helvetica-bold", size: 14.0)! ]
        let valueAttribute = [ NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 14.0)! ]
        
        let textString = NSMutableAttributedString(string: "\(key):", attributes: keyAttribute )
        
        let valueString = NSMutableAttributedString(string: "\(value)", attributes: valueAttribute )
        textString.append(valueString)
        return textString
    }
}
