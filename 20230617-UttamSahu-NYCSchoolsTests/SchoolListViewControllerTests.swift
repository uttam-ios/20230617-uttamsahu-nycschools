//
//  SchoolListViewControllerTests.swift
//  20230617-UttamSahu-NYCSchoolsTests
//
//  Created by Uttam Sahu on 18/06/23.
//

import XCTest
@testable import _0230617_UttamSahu_NYCSchools

class SchoolListViewControllerTests: XCTestCase {

    var viewController: SchoolListViewController!

    override func setUp() {
        super.setUp()
        viewController = SchoolListViewController()
        // Load the view hierarchy
        viewController.loadViewIfNeeded()
    }

    override func tearDown() {
        viewController = nil
        super.tearDown()
    }

    func testTableViewDataSource() {
        XCTAssertNotNil(viewController.tableView.dataSource, "Data source should not be nil")
        XCTAssertTrue(viewController.conforms(to: UITableViewDataSource.self), "ViewController should conform to UITableViewDataSource")
        XCTAssertTrue(viewController.responds(to: #selector(viewController.tableView(_:numberOfRowsInSection:))), "ViewController should implement the tableView(_:numberOfRowsInSection:) method")
        XCTAssertTrue(viewController.responds(to: #selector(viewController.tableView(_:cellForRowAt:))), "ViewController should implement the tableView(_:cellForRowAt:) method")
    }

    func testTableViewDelegate() {
        XCTAssertNotNil(viewController.tableView.delegate, "Delegate should not be nil")
        XCTAssertTrue(viewController.conforms(to: UITableViewDelegate.self), "ViewController should conform to UITableViewDelegate")
        XCTAssertTrue(viewController.responds(to: #selector(viewController.tableView(_:didSelectRowAt:))), "ViewController should implement the tableView(_:didSelectRowAt:) method")
    }

    func testTableCellRegistration() {
        
        // Creating model 
        var school = SchoolModel()
        school.school_name = "Liberation Diploma Plus High School"
        school.overview_paragraph = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform."
        school.school_10th_seats = "5"
        school.academicopportunities1 = "Sample Academic Opportunities"
        school.website = "www.theclintonschool.net"
        
        let schoolVM = SchoolViewModel(school: school)
        viewController.schoolViewModels.append(schoolVM)
        
        let cell = viewController.tableView(viewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is SchoolTableViewCell, "Cell should be of type SchoolTableViewCell")
        let cellReuseIdentifier = viewController.tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCellIdentifier")
        XCTAssertNotNil(cellReuseIdentifier, "Cell should be registered with the correct reuse identifier")
    }

}
