# 20230617-UttamSahu-NYCSchools



## Getting started

# NYC School iOS App (MVVM) with Unit Test

 

The NYC School iOS App is a sample application that demonstrates the use of the MVVM (Model-View-ViewModel) design pattern in iOS development. It allows users to browse and explore information about schools in New York City. Users can view a list of schools, access detailed information about each school, and visit their websites for more information.

 

## Features

 

The NYC School iOS App offers the following features:

 

1. **School List**: View a list of schools in New York City.

2. **School Details**: Access detailed information about the school, including its name, address, contact information, and academic programs.

3. **Website Visit**: Visit the website of each school directly from the app for more information.

4. **SAT Scores**: View SAT scores, including Math, Reading, and Writing, for schools that provide this data.

 

 

## Architecture - MVVM (Model-View-ViewModel)

 

The NYC School iOS App is built using the MVVM design pattern. The key components of the MVVM pattern used in this app are:

 

- **Model**: Represents the data and business logic of the app. In this app, models represent the school data and SAT scores.

- **View**: Displays the user interface and handles user interactions. It includes the school list, school details, and website visit screens.

- **ViewModel**: Acts as an intermediary between the View and the Model. It retrieves and processes data from the Model and provides it to the View for display. It also handles user interactions and communicates back to the Model as needed.

 

The use of the MVVM design pattern promotes separation of concerns, enhances testability, and provides a clear separation between the user interface and the underlying data.
